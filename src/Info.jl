# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
module Info

"""
Struct containing version information of the CaosDB server. Meant
mainly for internal usage; use `CaosDB.Connection.get_version_info` or
`CaosDB.Connection.print_version_info` to retrieve the version of the
connected CaosDB server.
"""
mutable struct _VersionInfo

    major::Cint
    minor::Cint
    patch::Cint
    pre_release::Cstring
    build::Cstring

    _VersionInfo() = new()

end

end # Info
