# Examples

**TODO** Fill the sections below once the parts have been implemented.

## Connect to a CaosDB server

See also the hints on the [Installation](@ref) and set-up of
CaosDB.jl. CaosDB.jl makes use of the C++ client of CaosDB, so in
order to connect to a CaosDB server you first have to configure the
connection via a configuration file as explained in the [documentation
of CaosDB's C++
client](https://docs.indiscale.com/caosdb-cpplib). Once the
configuration is set up, connecting to the server is as easy as

```julia
using CaosDB
connection = CaosDB.Connection.connect()
```

which will establish a connection and print the version of the server
you are connected to (since `connect` is exported, `connection =
connect()` works as well).

## Retrieve a Record

With CaosDB.jl you may use the same logic of creating a transaction
object, filling it with sub-requests, executing it, and retrieving the
result(s) as it is done in the [C++
client](https://docs.indiscale.com/coasdb-cpplib) (see below). This is
handy when wanting to have several requests in the same
transaction. However, most of the time, e.g., when retrieving one or
multiple Records, this is not necessary. CaosDB.jl provides helper
functions so you don't have to worry about transactions and their
executions. Assume you want to retrieve an Entity with id=123. This is done via

```julia
using CaosDB
entity = retrieve("123")
```

It is possible to specify the connection either by name or as a
connection object as created above in a second argument to
[`retrieve()`](@ref).

You can then use the getter methods like [`get_name`](@ref),
[`get_parents`](@ref), or [`get_properties`](@ref) to get the name,
the parents, or the properties of the retrieved entity.

Retrieving multiple entities works in the same way.  Type

```julia
results = retrieve(["123", "124", "125"])
```

to retrieve the entities with ids 123, 124, and 125. They can then be
accessed as `results[1]`, `results[2]`, and `results[3]`,
respectively. Note that [`retrieve`](@ref) returns a single entity
when called with a single id whereas a vector of entities is returned
when called with a list of ids.

The same (and more) can be achieved by building and executing the
transaction manually. This is done by

```julia
transaction = create_transaction()
add_retrieve_by_id(transaction, "123")
add_retrieve_by_id(transaction, "124")
add_retrieve_by_id(transaction, "125")
execute_transaction(transaction)
results = get_results(transaction)
```

Again, a connection can be specified by name or object as an optional
argument in the call to [`create_transaction`](@ref). Optionally, and
in the same way as in the C++ client, a result set can be obtained via
[`get_result_set`](@ref) which contains the resulting entities and
can, e.g., be checked for length. However, since it is not necessary
to interact with the result set and one usually is interested in the
recieved entities proper, the result set can be omitted. As above, it
is also possible to add multiple id retrievals at once.

```julia
transaction = create_transaction()
add_retrieve_by_id(transaction, ["123", "124", "125"])
execute_transaction(transaction)
results = get_results(transaction)
```

is equivalent to the above Code.

## Execute queries

Executing queries works very similar to the retrieval of entities via
ids. Again it is possible to create and execute a transaction manually
but most cases can be covered by the [`execute_query`](@ref) helper
function.

### FIND and SELECT queries

In general, entities can be found using CaosDB's query language like

```julia
results = execute_query("FIND RECORD WITH name Like 'some_pattern' AND id>122")
```

`results` is a (possibly empty) vector of entities that can be
inspected as above. Similar to `retrieve`, a connection can be
specified by name or by a connection object in
[`execute_query`](@ref). If none is specified, the default connection
is used. The same result is achieved by constructing the transaction
manually:

```julia
transaction = create_transaction()
add_query(transaction, "FIND RECORD WITH name Like 'some_pattern' AND id>122")
execute(transaction)
results = get_results(transaction)
```

!!! warning

	SELECT queries haven't been implemented in the C++ client yet and
	thus cannot be executed from CaosDB.jl right now. SELECT queries
	will be added in a future release.

### COUNT queries

COUNT queries are different from FIND or SELECT queries in two
ways. Firstly, they do not return entities but a single number which
is why, secondly, they do not have a result set that could be
returned. This is why they are currently not covered by the
`execute_query` function and have to be executed manually instead. The
result of the count is obtained using the [`get_count_result`](@ref)
function:

```julia
transaction = create_transaction()
add_query(transaction, "COUNT RECORD person WITH NAME LIKE '*Baggins'")
execute(transaction)
count_result = get_count_result(transaction)
```

## Insert, update, and delete entities

## Download a file
