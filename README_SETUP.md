# Set-up CaosDB.jl

## Installation

CaosDB.jl uses CaosDB's C interface which is accessed via `ccall`. So
to us CaosDB.jl, you must first build and install
[caosdb-cpplib](https://gitlab.com/caosdb/caosdb-cpplib) as explained
in its
[documentation](https://docs.indiscale.com/caosdb-cpplib). After
building the path to the shared library `libccaosdb` has to be made
known to Julia's
[`ccall`](https://docs.julialang.org/en/v1/manual/calling-c-and-fortran-code),
e.g., by adding its path to the `LD_LIBRARY_PATH`. Then, the CaosDB.jl
package can be accessed by

```julia-repl
julia> ]add "path/to/caosdb-julialib"
julia> using CaosDB
julia> connection = CaosDB.Connection.connect() # Enter host, port, path to SSL certificate, and credentials here
```

### Troubleshooting

On MacOS, if you encounter errors like

```
could not load library "libccaosdb"
dlopen(libccaosdb.dylib, 1): Library not loaded: libcaosdb.dylib
  Referenced from ~/.local/lib/libccaosdb.dylib
  Reason: image not found
```

(note that the library that couldn't be loaded is `libcaosdb`, i.e.,
the wrapped cpp library couldn't be loaded from the c interface) make
sure you have the location of `libcaosdb.dylib` and `libccaosdb.dylib`
in both, your `LD_LIBRARY_PATH` and your `DYLD_LIBRARY_PATH`.

## Configuration
The configuration is done the same way as the C++ client would do it. See 
[caosdb-cpplib](https://gitlab.com/caosdb/caosdb-cpplib).

## Tests

After installing, the unit tests can be executed by

```julia-repl
julia> ]activate "/path/to/caosdb-julialib"
julia> ]test
```

Note that you must have a CaosDB client configuration (see above) with
a valid `"default"` configuration for all tests to succeed.

## Code styling

We use
[JuliaFormatter.jl](https://github.com/domluna/JuliaFormatter.jl) for
automatic code formatting.  Simply install JuliaFormatter by

```julia-repl
julia> ]add JuliaFormatter
```

and execute

```sh
julia -e 'using JuliaFormatter; format(".", verbose=true)'
```

in the root directory of caosdb-julialib. You may leave out the
`verbose=true` option if you do not wish to see which files were
formatted.

## Documentation

The documentation can be built locally using
[Documenter.jl](https://github.com/JuliaDocs/Documenter.jl) by
executing

```sh
julia --color=yes docs/make.jl
```

Afterwards the built html files can be found in `docs/build`.
