#!/bin/bash

git clone https://gitlab.indiscale.com/caosdb/src/caosdb-cpplib.git
cd caosdb-cpplib
# check if there is a crooesponding cpp branch. Use the default branch
# if there isn't.
if (git show-ref --verify --quiet refs/heads/$CI_COMMIT_REF_NAME) ; then
    echo "git checkout $CI_COMMIT_REF_NAME"
    git checkout $CI_COMMIT_REF_NAME
else
    echo "git checkout $CPP_DEFAULT_BRANCH"
    git checkout $CPP_DEFAULT_BRANCH
fi
git submodule update --init --recursive
mkdir build
cd build
conan install .. --build=missing -s "compiler.libcxx=libstdc++11"
cmake ..
cmake --build .
cmake --install .
cd ../..
