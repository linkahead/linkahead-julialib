# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
module Utility

export get_ccaosdb_version, get_env_fallback

using ..CaosDB

"""
    get_env_fallback(var[, default])

Return the environmental variable `var` if it exists, `default`
otherwise. If no `default` is given an empty string is returned
instead.
"""
function get_env_fallback(var::AbstractString, default::AbstractString = "")

    ret = ccall(
        (:caosdb_utility_get_env_fallback, CaosDB.library_name),
        Cstring,
        (Cstring, Cstring),
        var,
        default,
    )

    return unsafe_string(ret)

end

"""
    function get_ccaosdb_version()

Return the version of the CaosDB C interface that is used by
CaosDB.jl.
"""
function get_ccaosdb_version()

    major =
        ccall((:caosdb_constants_LIBCAOSDB_VERSION_MAJOR, CaosDB.library_name), Cint, ())
    minor =
        ccall((:caosdb_constants_LIBCAOSDB_VERSION_MINOR, CaosDB.library_name), Cint, ())
    patch =
        ccall((:caosdb_constants_LIBCAOSDB_VERSION_PATCH, CaosDB.library_name), Cint, ())

    return VersionNumber("$major.$minor.$patch")
end

end # Utility
